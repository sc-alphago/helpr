:- discontiguous contain/2.
:- discontiguous meals/2.

contain(yellowrice, rice).
meals(yellowrice, breakfast).

contain(basicsushiricerecipe, rice).
meals(basicsushiricerecipe, breakfast).

contain(perfumedrice, rice).
meals(perfumedrice, breakfast).

contain(rainbowrice, rice).
meals(rainbowrice, breakfast).

contain(ricepudding, rice).
meals(ricepudding, breakfast).

contain(jasminerice, rice).
meals(jasminerice, breakfast).

contain(coconutrice, rice).
meals(coconutrice, breakfast).

contain(easyricepudding, rice).
meals(easyricepudding, breakfast).

contain(greenrice, rice).
meals(greenrice, breakfast).

contain(friedricewithham, rice).
meals(friedricewithham, breakfast).

contain(jeweledricesalad, rice).
meals(jeweledricesalad, breakfast).

contain(sushirice, rice).
meals(sushirice, breakfast).

contain(spanishrice, rice).
meals(spanishrice, breakfast).

contain(sushiriceandseasoning, rice).
meals(sushiriceandseasoning, breakfast).

contain(crispsushiricecakes, rice).
meals(crispsushiricecakes, breakfast).

contain(wildriceflatbreadrecipes, rice).
meals(wildriceflatbreadrecipes, breakfast).

contain(ricecookerricepudding, rice).
meals(ricecookerricepudding, breakfast).

contain(corianderrice, rice).
meals(corianderrice, breakfast).

contain(gingerricerecipes, rice).
meals(gingerricerecipes, breakfast).

contain(rainbowrice, rice).
meals(rainbowrice, breakfast).

contain(cashewrice, rice).
meals(cashewrice, breakfast).

contain(ricepilaf, rice).
meals(ricepilaf, breakfast).

contain(everydayrice, rice).
meals(everydayrice, breakfast).

contain(pineapplegingerrice, rice).
meals(pineapplegingerrice, breakfast).

contain(veggiefriedrice, rice).
meals(veggiefriedrice, breakfast).

contain(ricewithpomegranate, rice).
meals(ricewithpomegranate, breakfast).

contain(turkeyfriedrice, rice).
meals(turkeyfriedrice, breakfast).

contain(saffronwildrice, rice).
meals(saffronwildrice, breakfast).

contain(mangostickyrice, rice).
meals(mangostickyrice, breakfast).

contain(basmatirice, rice).
meals(basmatirice, breakfast).

contain(friedricewithham, rice).
meals(friedricewithham, breakfast).

contain(coconutrice, rice).
meals(coconutrice, breakfast).

contain(ricemilk, rice).
meals(ricemilk, breakfast).

contain(mangostickyrice, rice).
meals(mangostickyrice, breakfast).

contain(tomatorice, rice).
meals(tomatorice, breakfast).

contain(coconutrice, rice).
meals(coconutrice, breakfast).

contain(brownriceandedamame, rice).
meals(brownriceandedamame, breakfast).

contain(sizzledgingerrice, rice).
meals(sizzledgingerrice, breakfast).

contain(orangerice, rice).
meals(orangerice, breakfast).

contain(horchataricepudding, rice).
meals(horchataricepudding, breakfast).

contain(basmatiriceiranianwithtahdig, rice).
meals(basmatiriceiranianwithtahdig, breakfast).

contain(plainbasmatirice, rice).
meals(plainbasmatirice, breakfast).

contain(stickyricemango, rice).
meals(stickyricemango, breakfast).

contain(herbedricepilaf, rice).
meals(herbedricepilaf, breakfast).

contain(gingerwhiterice, rice).
meals(gingerwhiterice, breakfast).

contain(eggyfriedrice, rice).
meals(eggyfriedrice, breakfast).

contain(coconutrice, rice).
meals(coconutrice, breakfast).

contain(saffronrice, rice).
meals(saffronrice, breakfast).

contain(breakfastfriedrice, rice).
meals(breakfastfriedrice, breakfast).

contain(masterrecipeforsushirice, rice).
meals(masterrecipeforsushirice, breakfast).

contain(chineseblackrice, rice).
meals(chineseblackrice, breakfast).

contain(eggyfriedrice, rice).
meals(eggyfriedrice, breakfast).

contain(champoradochocolatericeporridge, rice).
meals(champoradochocolatericeporridge, breakfast).

contain(broccolirice, rice).
meals(broccolirice, breakfast).

contain(ricepudding, rice).
meals(ricepudding, breakfast).

contain(mexicanrice, rice).
meals(mexicanrice, breakfast).

contain(persianrice, rice).
meals(persianrice, breakfast).

contain(spinachrice, rice).
meals(spinachrice, breakfast).

contain(greekricepudding, rice).
meals(greekricepudding, breakfast).

contain(onionrice, rice).
meals(onionrice, breakfast).

contain(coconutjasminerice, rice).
meals(coconutjasminerice, breakfast).

contain(ricepudding, rice).
meals(ricepudding, breakfast).

contain(coconutrice, rice).
meals(coconutrice, breakfast).

contain(perfectsteamedrice, rice).
meals(perfectsteamedrice, breakfast).

contain(baconfriedrice, rice).
meals(baconfriedrice, breakfast).

contain(pepperedrice, rice).
meals(pepperedrice, breakfast).

contain(ricenoodles, rice).
meals(ricenoodles, breakfast).

contain(ricepilaf, rice).
meals(ricepilaf, breakfast).

contain(gingercilantrorice, rice).
meals(gingercilantrorice, breakfast).

contain(herbedjasminerice, rice).
meals(herbedjasminerice, breakfast).

contain(curriedrice, rice).
meals(curriedrice, breakfast).

contain(blackricepudding, rice).
meals(blackricepudding, breakfast).

contain(aromaticyellowrice, rice).
meals(aromaticyellowrice, breakfast).

contain(herbedrice, rice).
meals(herbedrice, breakfast).

contain(basmatirice, rice).
meals(basmatirice, breakfast).

contain(pepperyrice, rice).
meals(pepperyrice, breakfast).

contain(wildricecranberrystuffing, rice).
meals(wildricecranberrystuffing, breakfast).

contain(wildricesalad, rice).
meals(wildricesalad, breakfast).

contain(coldricesalad, rice).
meals(coldricesalad, breakfast).

contain(mexicanrice, rice).
meals(mexicanrice, breakfast).

contain(friedrice, rice).
meals(friedrice, breakfast).

contain(gingeredbrownrice, rice).
meals(gingeredbrownrice, breakfast).

contain(porkfriedricerecipes, rice).
meals(porkfriedricerecipes, breakfast).

contain(greenrice, rice).
meals(greenrice, breakfast).

contain(coconutricerecipes, rice).
meals(coconutricerecipes, breakfast).

contain(eggfriedrice, rice).
meals(eggfriedrice, breakfast).

contain(ricenoodleswithchicken, rice).
meals(ricenoodleswithchicken, breakfast).

contain(coconutjasminerice, rice).
meals(coconutjasminerice, breakfast).

contain(ricepilaf, rice).
meals(ricepilaf, breakfast).

contain(toastedcoconutrice, rice).
meals(toastedcoconutrice, breakfast).

contain(coconutricepudding, rice).
meals(coconutricepudding, breakfast).

contain(basmatiricepilaf, rice).
meals(basmatiricepilaf, breakfast).

contain(cinnamonricemilk, rice).
meals(cinnamonricemilk, breakfast).

contain(vegetablefriedrice, rice).
meals(vegetablefriedrice, breakfast).

contain(bakedbrownrice, rice).
meals(bakedbrownrice, breakfast).

contain(spicedtomatorice, rice).
meals(spicedtomatorice, breakfast).

contain(chineseclaypotrice, rice).
meals(chineseclaypotrice, breakfast).

contain(zestyspanishrice, rice).
meals(zestyspanishrice, breakfast).

contain(ricenoodlesalad, rice).
meals(ricenoodlesalad, breakfast).

contain(nuttybrownrice, rice).
meals(nuttybrownrice, breakfast).

contain(breadbakingpercenthydrationbread, bread).
meals(breadbakingpercenthydrationbread, breakfast).

contain(rusticitalianbread, bread).
meals(rusticitalianbread, breakfast).

contain(rusticbread, bread).
meals(rusticbread, breakfast).

contain(stuffedbread, bread).
meals(stuffedbread, breakfast).

contain(garlicbread, bread).
meals(garlicbread, breakfast).

contain(bread, bread).
meals(bread, breakfast).

contain(chickenfriedwithbreadcrumbs, bread).
meals(chickenfriedwithbreadcrumbs, breakfast).

contain(spanishtomatobread, bread).
meals(spanishtomatobread, breakfast).

contain(butterygarlicbread, bread).
meals(butterygarlicbread, breakfast).

contain(unleavenedgriddlebread, bread).
meals(unleavenedgriddlebread, breakfast).

contain(breakfastbread, bread).
meals(breakfastbread, breakfast).

contain(nokneadbreadrecipe, bread).
meals(nokneadbreadrecipe, breakfast).

contain(rusticbread, bread).
meals(rusticbread, breakfast).

contain(saintgeorgesbread, bread).
meals(saintgeorgesbread, breakfast).

contain(olivefrenchbread, bread).
meals(olivefrenchbread, breakfast).

contain(basicbreaddough, bread).
meals(basicbreaddough, breakfast).

contain(breadandmilk, bread).
meals(breadandmilk, breakfast).

contain(breadbakingcaterpillarbreadforhalloween, bread).
meals(breadbakingcaterpillarbreadforhalloween, breakfast).

contain(eggnogbreadpudding, bread).
meals(eggnogbreadpudding, breakfast).

contain(potatobread, bread).
meals(potatobread, breakfast).

contain(cornbreaddressing, bread).
meals(cornbreaddressing, breakfast).

contain(crustybread, bread).
meals(crustybread, breakfast).

contain(rusticbread, bread).
meals(rusticbread, breakfast).

contain(rusticbreadrecipes, bread).
meals(rusticbreadrecipes, breakfast).

contain(doonsbrownbread, bread).
meals(doonsbrownbread, breakfast).

contain(breadacountryloaf, bread).
meals(breadacountryloaf, breakfast).

contain(bloomerbread, bread).
meals(bloomerbread, breakfast).

contain(italianbread, bread).
meals(italianbread, breakfast).

contain(garlicbread, bread).
meals(garlicbread, breakfast).

contain(breadpudding, bread).
meals(breadpudding, breakfast).

contain(cheesypullapartbread, bread).
meals(cheesypullapartbread, breakfast).

contain(carrotbread, bread).
meals(carrotbread, breakfast).

contain(pitabread, bread).
meals(pitabread, breakfast).

contain(breadmachineryebread, bread).
meals(breadmachineryebread, breakfast).

contain(sandwichbread, bread).
meals(sandwichbread, breakfast).

contain(berrybreadpudding, bread).
meals(berrybreadpudding, breakfast).

contain(chocolateandwalnutbreadpanini, bread).
meals(chocolateandwalnutbreadpanini, breakfast).

contain(pestogarlicbread, bread).
meals(pestogarlicbread, breakfast).

contain(sausagebreadrecipes, bread).
meals(sausagebreadrecipes, breakfast).

contain(anadamabread, bread).
meals(anadamabread, breakfast).

contain(celerybread, bread).
meals(celerybread, breakfast).

contain(whitebread, bread).
meals(whitebread, breakfast).

contain(breadpudding, bread).
meals(breadpudding, breakfast).

contain(breadbakingsproutedwheatbread, bread).
meals(breadbakingsproutedwheatbread, breakfast).

contain(nutellabreadpudding, bread).
meals(nutellabreadpudding, breakfast).

contain(pitabread, bread).
meals(pitabread, breakfast).

contain(breadpuddingmugs, bread).
meals(breadpuddingmugs, breakfast).

contain(overnightbreadpudding, bread).
meals(overnightbreadpudding, breakfast).

contain(garlicbread, bread).
meals(garlicbread, breakfast).

contain(basilbread, bread).
meals(basilbread, breakfast).

contain(garlicbread, bread).
meals(garlicbread, breakfast).

contain(breadbakingsweetpotatobreadrecipe, bread).
meals(breadbakingsweetpotatobreadrecipe, breakfast).

contain(garlicherbbread, bread).
meals(garlicherbbread, breakfast).

contain(breadcrumbs, bread).
meals(breadcrumbs, breakfast).

contain(breadbakingwalnutapricotbreadrecipe, bread).
meals(breadbakingwalnutapricotbreadrecipe, breakfast).

contain(microwavebreadpudding, bread).
meals(microwavebreadpudding, breakfast).

contain(easygarlicbread, bread).
meals(easygarlicbread, breakfast).

contain(breadbakingyogurtandhoneybreadrecipe, bread).
meals(breadbakingyogurtandhoneybreadrecipe, breakfast).

contain(breadsauce, bread).
meals(breadsauce, breakfast).

contain(frenchbreadpudding, bread).
meals(frenchbreadpudding, breakfast).

contain(tomatobread, bread).
meals(tomatobread, breakfast).

contain(breadbakingcottagecheesedillbread, bread).
meals(breadbakingcottagecheesedillbread, breakfast).

contain(bostonbrownbread, bread).
meals(bostonbrownbread, breakfast).

contain(threebreadstuffing, bread).
meals(threebreadstuffing, breakfast).

contain(breadbakingitsjustwhitebreadrecipe, bread).
meals(breadbakingitsjustwhitebreadrecipe, breakfast).

contain(naanbread, bread).
meals(naanbread, breakfast).

contain(tearawaycheesybread, bread).
meals(tearawaycheesybread, breakfast).

contain(naanbread, bread).
meals(naanbread, breakfast).

contain(breadbakingseedywheatbread, bread).
meals(breadbakingseedywheatbread, breakfast).

contain(sourdoughbreadwithfennelseeds, bread).
meals(sourdoughbreadwithfennelseeds, breakfast).

contain(breadbakingcaramelapplebread, bread).
meals(breadbakingcaramelapplebread, breakfast).

contain(breadbakingmilkandhoneybreadwithsaffron, bread).
meals(breadbakingmilkandhoneybreadwithsaffron, breakfast).

contain(breadbakingtomatopestoswirlbread, bread).
meals(breadbakingtomatopestoswirlbread, breakfast).

contain(chocolatebread, bread).
meals(chocolatebread, breakfast).

contain(breadbakingirishwholemealwhitebread, bread).
meals(breadbakingirishwholemealwhitebread, breakfast).

contain(breadbakingtomatocheeseandbaconbread, bread).
meals(breadbakingtomatocheeseandbaconbread, breakfast).

contain(frenchbreadpizzas, bread).
meals(frenchbreadpizzas, breakfast).

contain(naanbreadpizza, bread).
meals(naanbreadpizza, breakfast).

contain(bltbreadsalad, bread).
meals(bltbreadsalad, breakfast).

contain(walnutbread, bread).
meals(walnutbread, breakfast).

contain(rosemarycornbread, bread).
meals(rosemarycornbread, breakfast).

contain(tartinebakeryscountrybread, bread).
meals(tartinebakeryscountrybread, breakfast).

contain(breadbakingapplebutterswirlbreadrecipe, bread).
meals(breadbakingapplebutterswirlbreadrecipe, breakfast).

contain(chocolatebreadpudding, bread).
meals(chocolatebreadpudding, breakfast).

contain(chunkygarlicbread, bread).
meals(chunkygarlicbread, breakfast).

contain(neworleansfrenchbread, bread).
meals(neworleansfrenchbread, breakfast).

contain(chocolatebreadpudding, bread).
meals(chocolatebreadpudding, breakfast).

contain(honeybananabreadrecipe, bread).
meals(honeybananabreadrecipe, breakfast).

contain(cheesychivebread, bread).
meals(cheesychivebread, breakfast).

contain(pitabread, bread).
meals(pitabread, breakfast).

contain(thebreadbiblessesamewhitebreadrecipe, bread).
meals(thebreadbiblessesamewhitebreadrecipe, breakfast).

contain(pbandjbreadcrustpudding, bread).
meals(pbandjbreadcrustpudding, breakfast).

contain(simplegarlicbreadrecipes, bread).
meals(simplegarlicbreadrecipes, breakfast).

contain(asiagopepperbreadrecipes, bread).
meals(asiagopepperbreadrecipes, breakfast).

contain(crawfishbread, bread).
meals(crawfishbread, breakfast).

contain(breadwreath, bread).
meals(breadwreath, breakfast).

contain(silkenbreadandparmesansoup, bread).
meals(silkenbreadandparmesansoup, breakfast).

contain(breadbakingsodabreadwithdriedcranberriesrecipe, bread).
meals(breadbakingsodabreadwithdriedcranberriesrecipe, breakfast).

contain(easterbunnybread, bread).
meals(easterbunnybread, breakfast).

contain(breadmachinewholewheatandpeanutbutterbread, bread).
meals(breadmachinewholewheatandpeanutbutterbread, breakfast).

contain(ricecerealbars, cereal).
meals(ricecerealbars, breakfast).

contain(cinnnamoncerealswirlbread, cereal).
meals(cinnnamoncerealswirlbread, breakfast).

contain(smorecerealbarsplease, cereal).
meals(smorecerealbarsplease, breakfast).

contain(chocolatecoveredcereal, cereal).
meals(chocolatecoveredcereal, breakfast).

contain(spicycerealandnutmixrecipes, cereal).
meals(spicycerealandnutmixrecipes, breakfast).

contain(chewynutandcerealbars, cereal).
meals(chewynutandcerealbars, breakfast).

contain(chocolatepeanutbuttercerealbars, cereal).
meals(chocolatepeanutbuttercerealbars, breakfast).

contain(momofukumilkbarscerealmilkicecreamrecipes, cereal).
meals(momofukumilkbarscerealmilkicecreamrecipes, breakfast).

contain(crunchypeanutbuttercerealbarsrecipe, cereal).
meals(crunchypeanutbuttercerealbarsrecipe, breakfast).

contain(chocolatecerealbars, cereal).
meals(chocolatecerealbars, breakfast).

contain(cerealmilkicecream, cereal).
meals(cerealmilkicecream, breakfast).

contain(milkandcerealbars, cereal).
meals(milkandcerealbars, breakfast).

contain(chocolatecandycanesnowflakes, cereal).
meals(chocolatecandycanesnowflakes, breakfast).

contain(ricecrispyfriendshiptreat, cereal).
meals(ricecrispyfriendshiptreat, breakfast).

contain(cerealicepops, cereal).
meals(cerealicepops, breakfast).

contain(wholegraincerealpartymixrecipes, cereal).
meals(wholegraincerealpartymixrecipes, breakfast).

contain(sugarpoprecipes, cereal).
meals(sugarpoprecipes, breakfast).

contain(peanutbutterchocolatecerealbars, cereal).
meals(peanutbutterchocolatecerealbars, breakfast).

contain(chocolateoverloadmix, cereal).
meals(chocolateoverloadmix, breakfast).

contain(spicedchocolatepuddingwithcaramelcrispedricecereal, cereal).
meals(spicedchocolatepuddingwithcaramelcrispedricecereal, breakfast).

contain(amaranthbreakfastcerealwithblueberriesrecipes, cereal).
meals(amaranthbreakfastcerealwithblueberriesrecipes, breakfast).

contain(marshmallowcerealbars, cereal).
meals(marshmallowcerealbars, breakfast).

contain(ricekrispiestreatsbirthdaycakerecipes, cereal).
meals(ricekrispiestreatsbirthdaycakerecipes, breakfast).

contain(spicedpumpkinandgoldengrahamscerealtriflerecipe, cereal).
meals(spicedpumpkinandgoldengrahamscerealtriflerecipe, breakfast).

contain(cerealkillershake, cereal).
meals(cerealkillershake, breakfast).

contain(strawberryyogurtcreampiewithcerealcrust, cereal).
meals(strawberryyogurtcreampiewithcerealcrust, breakfast).

contain(cerealmixwitholiveoilandparmesan, cereal).
meals(cerealmixwitholiveoilandparmesan, breakfast).

contain(cerealmixwitholiveoilandparmesanrecipes, cereal).
meals(cerealmixwitholiveoilandparmesanrecipes, breakfast).

contain(powerboostcereal, cereal).
meals(powerboostcereal, breakfast).

contain(homemadecracklycandybars, cereal).
meals(homemadecracklycandybars, breakfast).

contain(diygrapenutscerealrecipe, cereal).
meals(diygrapenutscerealrecipe, breakfast).

contain(cerealbowlsmoothie, cereal).
meals(cerealbowlsmoothie, breakfast).

contain(cerealbowlcake, cereal).
meals(cerealbowlcake, breakfast).

contain(peanutbutterchocolateoatmealcerealbarsrecipes, cereal).
meals(peanutbutterchocolateoatmealcerealbarsrecipes, breakfast).

contain(confettisquaresrecipes, cereal).
meals(confettisquaresrecipes, breakfast).

contain(roastedrootvegetableswithsoycaramelandcrispedrice, cereal).
meals(roastedrootvegetableswithsoycaramelandcrispedrice, breakfast).

contain(cerealkillerbars, cereal).
meals(cerealkillerbars, breakfast).

contain(cheesycerealcrackers, cereal).
meals(cheesycerealcrackers, breakfast).

contain(mueslibreakfastcerealrecipes, cereal).
meals(mueslibreakfastcerealrecipes, breakfast).

contain(hotfruitspicecereal, cereal).
meals(hotfruitspicecereal, breakfast).

contain(diyblueberrycerealbarsrecipe, cereal).
meals(diyblueberrycerealbarsrecipe, breakfast).

contain(peanutbuttercerealtreats, cereal).
meals(peanutbuttercerealtreats, breakfast).

contain(minutemultigraincereal, cereal).
meals(minutemultigraincereal, breakfast).

contain(strawberrycerealtreats, cereal).
meals(strawberrycerealtreats, breakfast).

contain(microwavesnackmix, cereal).
meals(microwavesnackmix, breakfast).

contain(instantpartymix, cereal).
meals(instantpartymix, breakfast).

contain(fruitypebblesbananabelgianwaffles, cereal).
meals(fruitypebblesbananabelgianwaffles, breakfast).

contain(cerealmilkpudding, cereal).
meals(cerealmilkpudding, breakfast).

contain(santasnackmixrecipes, cereal).
meals(santasnackmixrecipes, breakfast).

contain(hollydaycheesecake, cereal).
meals(hollydaycheesecake, breakfast).

contain(potogold, cereal).
meals(potogold, breakfast).

contain(smorescerealtreats, cereal).
meals(smorescerealtreats, breakfast).

contain(creamygrainwithgoatcheeseyogurtandfig, cereal).
meals(creamygrainwithgoatcheeseyogurtandfig, breakfast).

contain(slowcookerchexmix, cereal).
meals(slowcookerchexmix, breakfast).

contain(crunchycerealtrailmix, cereal).
meals(crunchycerealtrailmix, breakfast).

contain(cometswhitechocolatecrunch, cereal).
meals(cometswhitechocolatecrunch, breakfast).

contain(pinkpuffedricecerealhearts, cereal).
meals(pinkpuffedricecerealhearts, breakfast).

contain(magicreindeerfoodrecipes, cereal).
meals(magicreindeerfoodrecipes, breakfast).

contain(crunchycerealchickenfingers, cereal).
meals(crunchycerealchickenfingers, breakfast).

contain(grandmakatiesfrozendessert, cereal).
meals(grandmakatiesfrozendessert, breakfast).

contain(sunnyscerealtrailmix, cereal).
meals(sunnyscerealtrailmix, breakfast).

contain(familyfavoritepartymixrecipes, cereal).
meals(familyfavoritepartymixrecipes, breakfast).

contain(sunnyscerealconfetticookies, cereal).
meals(sunnyscerealconfetticookies, breakfast).

contain(cerealandpotatochipbars, cereal).
meals(cerealandpotatochipbars, breakfast).

contain(chocolatecoconutcerealtreats, cereal).
meals(chocolatecoconutcerealtreats, breakfast).

contain(peanutbutterchocolateandpretzelcerealtreats, cereal).
meals(peanutbutterchocolateandpretzelcerealtreats, breakfast).

contain(nutfreeranchflavoredcrispycerealsnackmix, cereal).
meals(nutfreeranchflavoredcrispycerealsnackmix, breakfast).

contain(wholehotapplecereal, cereal).
meals(wholehotapplecereal, breakfast).

contain(tripleberrycerealbars, cereal).
meals(tripleberrycerealbars, breakfast).

contain(hungrygirlnola, cereal).
meals(hungrygirlnola, breakfast).

contain(appleraisincrackedwheatcereal, cereal).
meals(appleraisincrackedwheatcereal, breakfast).

contain(danascrispycoconutchicken, cereal).
meals(danascrispycoconutchicken, breakfast).

contain(chocolatescotcheroos, cereal).
meals(chocolatescotcheroos, breakfast).

contain(pizzapartymix, cereal).
meals(pizzapartymix, breakfast).

contain(friedicecreamwithcerealcrust, cereal).
meals(friedicecreamwithcerealcrust, breakfast).

contain(pralinechocolatesnackmix, cereal).
meals(pralinechocolatesnackmix, breakfast).

contain(partymix, cereal).
meals(partymix, breakfast).

contain(sweetandspicysnackmix, cereal).
meals(sweetandspicysnackmix, breakfast).

contain(caramelizedcrispyricebark, cereal).
meals(caramelizedcrispyricebark, breakfast).

contain(peanuttypumpkinspicetoffeebars, cereal).
meals(peanuttypumpkinspicetoffeebars, breakfast).

contain(patrioticcheesecakeparfaits, cereal).
meals(patrioticcheesecakeparfaits, breakfast).

contain(hotricecerealwithnutsandraisinsrecipes, cereal).
meals(hotricecerealwithnutsandraisinsrecipes, breakfast).

contain(peanutbuttercrispybarsrecipes, cereal).
meals(peanutbuttercrispybarsrecipes, breakfast).

contain(multigrainhotcerealwithcranberriesandorangesrecipes, cereal).
meals(multigrainhotcerealwithcranberriesandorangesrecipes, breakfast).

contain(cocoapebbleschurros, cereal).
meals(cocoapebbleschurros, breakfast).

contain(luckycharmsicecreamsandwiches, cereal).
meals(luckycharmsicecreamsandwiches, breakfast).

contain(limecheesecakewithfruitypebblescrust, cereal).
meals(limecheesecakewithfruitypebblescrust, breakfast).

contain(fruitypebblesicecreamtreats, cereal).
meals(fruitypebblesicecreamtreats, breakfast).

contain(crispyoatsquares, cereal).
meals(crispyoatsquares, breakfast).

contain(nochurnvanillaicecream, cereal).
meals(nochurnvanillaicecream, breakfast).

contain(icecreamtreasures, cereal).
meals(icecreamtreasures, breakfast).

contain(reindeerchow, cereal).
meals(reindeerchow, breakfast).

contain(gonefishinsnackmix, cereal).
meals(gonefishinsnackmix, breakfast).

contain(peppermintbark, cereal).
meals(peppermintbark, breakfast).

contain(everydaygranolarecipes, cereal).
meals(everydaygranolarecipes, breakfast).

contain(chocolatekidsmix, cereal).
meals(chocolatekidsmix, breakfast).

contain(applepiepartymix, cereal).
meals(applepiepartymix, breakfast).

contain(blizzardblitzpartymix, cereal).
meals(blizzardblitzpartymix, breakfast).

contain(doeneesnutellabarsrecipe, cereal).
meals(doeneesnutellabarsrecipe, breakfast).

contain(brownricecerealwithvanillacreamandberries, cereal).
meals(brownricecerealwithvanillacreamandberries, breakfast).

contain(chickenpaprikash, chicken).
meals(chickenpaprikash, lunch).
meals(chickenpaprikash, dinner).

contain(bakedchicken, chicken).
meals(bakedchicken, lunch).
meals(bakedchicken, dinner).

contain(catalanchicken, chicken).
meals(catalanchicken, lunch).
meals(catalanchicken, dinner).

contain(chickenteriyaki, chicken).
meals(chickenteriyaki, lunch).
meals(chickenteriyaki, dinner).

contain(citrusroastedchicken, chicken).
meals(citrusroastedchicken, lunch).
meals(citrusroastedchicken, dinner).

contain(persianchicken, chicken).
meals(persianchicken, lunch).
meals(persianchicken, dinner).

contain(chickenpiccata, chicken).
meals(chickenpiccata, lunch).
meals(chickenpiccata, dinner).

contain(roastchicken, chicken).
meals(roastchicken, lunch).
meals(roastchicken, dinner).

contain(kreplachchickendumplings, chicken).
meals(kreplachchickendumplings, lunch).
meals(kreplachchickendumplings, dinner).

contain(chickencacciatore, chicken).
meals(chickencacciatore, lunch).
meals(chickencacciatore, dinner).

contain(roastchickenwithchickenlivers, chicken).
meals(roastchickenwithchickenlivers, lunch).
meals(roastchickenwithchickenlivers, dinner).

contain(chickenvesuvio, chicken).
meals(chickenvesuvio, lunch).
meals(chickenvesuvio, dinner).

contain(chickenhash, chicken).
meals(chickenhash, lunch).
meals(chickenhash, dinner).

contain(chickenpeppers, chicken).
meals(chickenpeppers, lunch).
meals(chickenpeppers, dinner).

contain(southwesternchickensoup, chicken).
meals(southwesternchickensoup, lunch).
meals(southwesternchickensoup, dinner).

contain(ultimateroastchicken, chicken).
meals(ultimateroastchicken, lunch).
meals(ultimateroastchicken, dinner).

contain(chickenpiccata, chicken).
meals(chickenpiccata, lunch).
meals(chickenpiccata, dinner).

contain(chickencordonbleu, chicken).
meals(chickencordonbleu, lunch).
meals(chickencordonbleu, dinner).

contain(chickenpie, chicken).
meals(chickenpie, lunch).
meals(chickenpie, dinner).

contain(chickenpoofs, chicken).
meals(chickenpoofs, lunch).
meals(chickenpoofs, dinner).

contain(provencalchicken, chicken).
meals(provencalchicken, lunch).
meals(provencalchicken, dinner).

contain(pulledchicken, chicken).
meals(pulledchicken, lunch).
meals(pulledchicken, dinner).

contain(lemonchicken, chicken).
meals(lemonchicken, lunch).
meals(lemonchicken, dinner).

contain(braisedchicken, chicken).
meals(braisedchicken, lunch).
meals(braisedchicken, dinner).

contain(chickenparmigianarecipes, chicken).
meals(chickenparmigianarecipes, lunch).
meals(chickenparmigianarecipes, dinner).

contain(thomatochicken, chicken).
meals(thomatochicken, lunch).
meals(thomatochicken, dinner).

contain(circassianchicken, chicken).
meals(circassianchicken, lunch).
meals(circassianchicken, dinner).

contain(chickenpaprika, chicken).
meals(chickenpaprika, lunch).
meals(chickenpaprika, dinner).

contain(lemonchicken, chicken).
meals(lemonchicken, lunch).
meals(lemonchicken, dinner).

contain(chickensoup, chicken).
meals(chickensoup, lunch).
meals(chickensoup, dinner).

contain(roastchicken, chicken).
meals(roastchicken, lunch).
meals(roastchicken, dinner).

contain(firecrackerchicken, chicken).
meals(firecrackerchicken, lunch).
meals(firecrackerchicken, dinner).

contain(champagnecanchicken, chicken).
meals(champagnecanchicken, lunch).
meals(champagnecanchicken, dinner).

contain(chickencarbonara, chicken).
meals(chickencarbonara, lunch).
meals(chickencarbonara, dinner).

contain(barbecuedchickenpizza, chicken).
meals(barbecuedchickenpizza, lunch).
meals(barbecuedchickenpizza, dinner).

contain(lemoncaperchicken, chicken).
meals(lemoncaperchicken, lunch).
meals(lemoncaperchicken, dinner).

contain(chickentacos, chicken).
meals(chickentacos, lunch).
meals(chickentacos, dinner).

contain(chickensoup, chicken).
meals(chickensoup, lunch).
meals(chickensoup, dinner).

contain(chickencurry, chicken).
meals(chickencurry, lunch).
meals(chickencurry, dinner).

contain(chickencacciatore, chicken).
meals(chickencacciatore, lunch).
meals(chickencacciatore, dinner).

contain(basicroastchicken, chicken).
meals(basicroastchicken, lunch).
meals(basicroastchicken, dinner).

contain(stuffedchickencaprese, chicken).
meals(stuffedchickencaprese, lunch).
meals(stuffedchickencaprese, dinner).

contain(chickensatay, chicken).
meals(chickensatay, lunch).
meals(chickensatay, dinner).

contain(chickenpiccata, chicken).
meals(chickenpiccata, lunch).
meals(chickenpiccata, dinner).

contain(easychickensoup, chicken).
meals(easychickensoup, lunch).
meals(easychickensoup, dinner).

contain(delicatechickensoup, chicken).
meals(delicatechickensoup, lunch).
meals(delicatechickensoup, dinner).

contain(chickenfricassee, chicken).
meals(chickenfricassee, lunch).
meals(chickenfricassee, dinner).

contain(chickenfricassee, chicken).
meals(chickenfricassee, lunch).
meals(chickenfricassee, dinner).

contain(cornbreadchickenbake, chicken).
meals(cornbreadchickenbake, lunch).
meals(cornbreadchickenbake, dinner).

contain(margaritachicken, chicken).
meals(margaritachicken, lunch).
meals(margaritachicken, dinner).

contain(chickenpotpies, chicken).
meals(chickenpotpies, lunch).
meals(chickenpotpies, dinner).

contain(quinoacrustedchicken, chicken).
meals(quinoacrustedchicken, lunch).
meals(quinoacrustedchicken, dinner).

contain(barbecuedchicken, chicken).
meals(barbecuedchicken, lunch).
meals(barbecuedchicken, dinner).

contain(chickenpiccata, chicken).
meals(chickenpiccata, lunch).
meals(chickenpiccata, dinner).

contain(unfriedchicken, chicken).
meals(unfriedchicken, lunch).
meals(unfriedchicken, dinner).

contain(chickenroulades, chicken).
meals(chickenroulades, lunch).
meals(chickenroulades, dinner).

contain(twistinchicken, chicken).
meals(twistinchicken, lunch).
meals(twistinchicken, dinner).

contain(chickenpie, chicken).
meals(chickenpie, lunch).
meals(chickenpie, dinner).

contain(panfriedchicken, chicken).
meals(panfriedchicken, lunch).
meals(panfriedchicken, dinner).

contain(peanutroastedchicken, chicken).
meals(peanutroastedchicken, lunch).
meals(peanutroastedchicken, dinner).

contain(lemonchicken, chicken).
meals(lemonchicken, lunch).
meals(lemonchicken, dinner).

contain(chickenpaillard, chicken).
meals(chickenpaillard, lunch).
meals(chickenpaillard, dinner).

contain(chickenpiccata, chicken).
meals(chickenpiccata, lunch).
meals(chickenpiccata, dinner).

contain(braisedchickensausages, chicken).
meals(braisedchickensausages, lunch).
meals(braisedchickensausages, dinner).

contain(chickensaltimbocca, chicken).
meals(chickensaltimbocca, lunch).
meals(chickensaltimbocca, dinner).

contain(chickencarbonara, chicken).
meals(chickencarbonara, lunch).
meals(chickencarbonara, dinner).

contain(chickenanddumplings, chicken).
meals(chickenanddumplings, lunch).
meals(chickenanddumplings, dinner).

contain(lemonchicken, chicken).
meals(lemonchicken, lunch).
meals(lemonchicken, dinner).

contain(chickenfrancese, chicken).
meals(chickenfrancese, lunch).
meals(chickenfrancese, dinner).

contain(chickenpiccata, chicken).
meals(chickenpiccata, lunch).
meals(chickenpiccata, dinner).

contain(greekchicken, chicken).
meals(greekchicken, lunch).
meals(greekchicken, dinner).

contain(chickensaltimbocca, chicken).
meals(chickensaltimbocca, lunch).
meals(chickensaltimbocca, dinner).

contain(chickenpiccata, chicken).
meals(chickenpiccata, lunch).
meals(chickenpiccata, dinner).

contain(kickinchicken, chicken).
meals(kickinchicken, lunch).
meals(kickinchicken, dinner).

contain(chickenwithmushrooms, chicken).
meals(chickenwithmushrooms, lunch).
meals(chickenwithmushrooms, dinner).

contain(chickenchowder, chicken).
meals(chickenchowder, lunch).
meals(chickenchowder, dinner).

contain(drunkenchickenrecipe, chicken).
meals(drunkenchickenrecipe, lunch).
meals(drunkenchickenrecipe, dinner).

contain(chickentinga, chicken).
meals(chickentinga, lunch).
meals(chickentinga, dinner).

contain(chickensoup, chicken).
meals(chickensoup, lunch).
meals(chickensoup, dinner).

contain(chickenbiryani, chicken).
meals(chickenbiryani, lunch).
meals(chickenbiryani, dinner).

contain(chickenpiccata, chicken).
meals(chickenpiccata, lunch).
meals(chickenpiccata, dinner).

contain(roastchicken, chicken).
meals(roastchicken, lunch).
meals(roastchicken, dinner).

contain(chickenmarsala, chicken).
meals(chickenmarsala, lunch).
meals(chickenmarsala, dinner).

contain(chickensaltimboccarecipes, chicken).
meals(chickensaltimboccarecipes, lunch).
meals(chickensaltimboccarecipes, dinner).

contain(circassianchicken, chicken).
meals(circassianchicken, lunch).
meals(circassianchicken, dinner).

contain(creamedchickenrecipes, chicken).
meals(creamedchickenrecipes, lunch).
meals(creamedchickenrecipes, dinner).

contain(chickenstoup, chicken).
meals(chickenstoup, lunch).
meals(chickenstoup, dinner).

contain(chickentetrazzini, chicken).
meals(chickentetrazzini, lunch).
meals(chickentetrazzini, dinner).

contain(chickentacos, chicken).
meals(chickentacos, lunch).
meals(chickentacos, dinner).

contain(citruschickenandrice, chicken).
meals(citruschickenandrice, lunch).
meals(citruschickenandrice, dinner).

contain(roastchicken, chicken).
meals(roastchicken, lunch).
meals(roastchicken, dinner).

contain(roastchicken, chicken).
meals(roastchicken, lunch).
meals(roastchicken, dinner).

contain(chickenndumplings, chicken).
meals(chickenndumplings, lunch).
meals(chickenndumplings, dinner).

contain(cardamomhoneychicken, chicken).
meals(cardamomhoneychicken, lunch).
meals(cardamomhoneychicken, dinner).

contain(chickenchili, chicken).
meals(chickenchili, lunch).
meals(chickenchili, dinner).

contain(chickencacciatore, chicken).
meals(chickencacciatore, lunch).
meals(chickencacciatore, dinner).

contain(classicbakedchicken, chicken).
meals(classicbakedchicken, lunch).
meals(classicbakedchicken, dinner).

contain(chickensaltimbocca, chicken).
meals(chickensaltimbocca, lunch).
meals(chickensaltimbocca, dinner).

contain(roastsirloinofbeef, beef).
meals(roastsirloinofbeef, lunch).
meals(roastsirloinofbeef, dinner).

contain(beefbrisket, beef).
meals(beefbrisket, lunch).
meals(beefbrisket, dinner).

contain(barbecuedbeefbrisket, beef).
meals(barbecuedbeefbrisket, lunch).
meals(barbecuedbeefbrisket, dinner).

contain(beeftacos, beef).
meals(beeftacos, lunch).
meals(beeftacos, dinner).

contain(beeftenderloin, beef).
meals(beeftenderloin, lunch).
meals(beeftenderloin, dinner).

contain(hacheedutchbeefstew, beef).
meals(hacheedutchbeefstew, lunch).
meals(hacheedutchbeefstew, dinner).

contain(beefandnoodlesrecipes, beef).
meals(beefandnoodlesrecipes, lunch).
meals(beefandnoodlesrecipes, dinner).

contain(portabellobeefstew, beef).
meals(portabellobeefstew, lunch).
meals(portabellobeefstew, dinner).

contain(beefstew, beef).
meals(beefstew, lunch).
meals(beefstew, dinner).

contain(beefstew, beef).
meals(beefstew, lunch).
meals(beefstew, dinner).

contain(roastribofbeef, beef).
meals(roastribofbeef, lunch).
meals(roastribofbeef, dinner).

contain(mincedbeefpie, beef).
meals(mincedbeefpie, lunch).
meals(mincedbeefpie, dinner).

contain(misobeefnoodlesoup, beef).
meals(misobeefnoodlesoup, lunch).
meals(misobeefnoodlesoup, dinner).

contain(beefsliders, beef).
meals(beefsliders, lunch).
meals(beefsliders, dinner).

contain(beefstroganoff, beef).
meals(beefstroganoff, lunch).
meals(beefstroganoff, dinner).

contain(beeftea, beef).
meals(beeftea, lunch).
meals(beeftea, dinner).

contain(homestyleroastbeef, beef).
meals(homestyleroastbeef, lunch).
meals(homestyleroastbeef, dinner).

contain(cornedbeefhash, beef).
meals(cornedbeefhash, lunch).
meals(cornedbeefhash, dinner).

contain(roastedbeefbrisket, beef).
meals(roastedbeefbrisket, lunch).
meals(roastedbeefbrisket, dinner).

contain(beefribs, beef).
meals(beefribs, lunch).
meals(beefribs, dinner).

contain(beefbiryanirecipes, beef).
meals(beefbiryanirecipes, lunch).
meals(beefbiryanirecipes, dinner).

contain(beefcarbonnade, beef).
meals(beefcarbonnade, lunch).
meals(beefcarbonnade, dinner).

contain(beefandnoodles, beef).
meals(beefandnoodles, lunch).
meals(beefandnoodles, dinner).

contain(basicbeef, beef).
meals(basicbeef, lunch).
meals(basicbeef, dinner).

contain(homemadecornedbeef, beef).
meals(homemadecornedbeef, lunch).
meals(homemadecornedbeef, dinner).

contain(beefcarbonnaderecipes, beef).
meals(beefcarbonnaderecipes, lunch).
meals(beefcarbonnaderecipes, dinner).

contain(mexicanbraisedbeeftacos, beef).
meals(mexicanbraisedbeeftacos, lunch).
meals(mexicanbraisedbeeftacos, dinner).

contain(beefchili, beef).
meals(beefchili, lunch).
meals(beefchili, dinner).

contain(beefbourguignon, beef).
meals(beefbourguignon, lunch).
meals(beefbourguignon, dinner).

contain(slowcookerbeefstroganoff, beef).
meals(slowcookerbeefstroganoff, lunch).
meals(slowcookerbeefstroganoff, dinner).

contain(quickbeefstroganoff, beef).
meals(quickbeefstroganoff, lunch).
meals(quickbeefstroganoff, dinner).

contain(garlicbeef, beef).
meals(garlicbeef, lunch).
meals(garlicbeef, dinner).

contain(beefmedallions, beef).
meals(beefmedallions, lunch).
meals(beefmedallions, dinner).

contain(beefempanadas, beef).
meals(beefempanadas, lunch).
meals(beefempanadas, dinner).

contain(italianbeef, beef).
meals(italianbeef, lunch).
meals(italianbeef, dinner).

contain(beefbrisket, beef).
meals(beefbrisket, lunch).
meals(beefbrisket, dinner).

contain(cornedbeefhash, beef).
meals(cornedbeefhash, lunch).
meals(cornedbeefhash, dinner).

contain(irishbeefstew, beef).
meals(irishbeefstew, lunch).
meals(irishbeefstew, dinner).

contain(orangebeef, beef).
meals(orangebeef, lunch).
meals(orangebeef, dinner).

contain(garlicbeef, beef).
meals(garlicbeef, lunch).
meals(garlicbeef, dinner).

contain(beefnoodlesoup, beef).
meals(beefnoodlesoup, lunch).
meals(beefnoodlesoup, dinner).

contain(beefgulasch, beef).
meals(beefgulasch, lunch).
meals(beefgulasch, dinner).

contain(beefvegetablesoup, beef).
meals(beefvegetablesoup, lunch).
meals(beefvegetablesoup, dinner).

contain(beefstew, beef).
meals(beefstew, lunch).
meals(beefstew, dinner).

contain(beefbourguignon, beef).
meals(beefbourguignon, lunch).
meals(beefbourguignon, dinner).

contain(beefpaprikash, beef).
meals(beefpaprikash, lunch).
meals(beefpaprikash, dinner).

contain(beeftenderloin, beef).
meals(beeftenderloin, lunch).
meals(beeftenderloin, dinner).

contain(pappardellewithbeef, beef).
meals(pappardellewithbeef, lunch).
meals(pappardellewithbeef, dinner).

contain(beefstroganoff, beef).
meals(beefstroganoff, lunch).
meals(beefstroganoff, dinner).

contain(beefstroganoff, beef).
meals(beefstroganoff, lunch).
meals(beefstroganoff, dinner).

contain(irishbeefstew, beef).
meals(irishbeefstew, lunch).
meals(irishbeefstew, dinner).

contain(swansonandregroastbeefwithgravy, beef).
meals(swansonandregroastbeefwithgravy, lunch).
meals(swansonandregroastbeefwithgravy, dinner).

contain(vietnamesebeefsoup, beef).
meals(vietnamesebeefsoup, lunch).
meals(vietnamesebeefsoup, dinner).

contain(pappardellewithbeef, beef).
meals(pappardellewithbeef, lunch).
meals(pappardellewithbeef, dinner).

contain(beeffettuccinedinner, beef).
meals(beeffettuccinedinner, lunch).
meals(beeffettuccinedinner, dinner).

contain(beefstroganoff, beef).
meals(beefstroganoff, lunch).
meals(beefstroganoff, dinner).

contain(braisedbeefbrisketrecipes, beef).
meals(braisedbeefbrisketrecipes, lunch).
meals(braisedbeefbrisketrecipes, dinner).

contain(beefstewcasserole, beef).
meals(beefstewcasserole, lunch).
meals(beefstewcasserole, dinner).

contain(beefgoulashsoup, beef).
meals(beefgoulashsoup, lunch).
meals(beefgoulashsoup, dinner).

contain(beefstroganov, beef).
meals(beefstroganov, lunch).
meals(beefstroganov, dinner).

contain(filletofbeef, beef).
meals(filletofbeef, lunch).
meals(filletofbeef, dinner).

contain(beefvegetablecasserole, beef).
meals(beefvegetablecasserole, lunch).
meals(beefvegetablecasserole, dinner).

contain(gingerbeefsalad, beef).
meals(gingerbeefsalad, lunch).
meals(gingerbeefsalad, dinner).

contain(gingerbeefsalad, beef).
meals(gingerbeefsalad, lunch).
meals(gingerbeefsalad, dinner).

contain(beefpho, beef).
meals(beefpho, lunch).
meals(beefpho, dinner).

contain(glazedcornedbeef, beef).
meals(glazedcornedbeef, lunch).
meals(glazedcornedbeef, dinner).

contain(creamedbeef, beef).
meals(creamedbeef, lunch).
meals(creamedbeef, dinner).

contain(beefbourguignon, beef).
meals(beefbourguignon, lunch).
meals(beefbourguignon, dinner).

contain(teriyakibeef, beef).
meals(teriyakibeef, lunch).
meals(teriyakibeef, dinner).

contain(beefstroganoff, beef).
meals(beefstroganoff, lunch).
meals(beefstroganoff, dinner).

contain(beefandmushroomburgundy, beef).
meals(beefandmushroomburgundy, lunch).
meals(beefandmushroomburgundy, dinner).

contain(filetofbeef, beef).
meals(filetofbeef, lunch).
meals(filetofbeef, dinner).

contain(patsroastbeef, beef).
meals(patsroastbeef, lunch).
meals(patsroastbeef, dinner).

contain(beeflomein, beef).
meals(beeflomein, lunch).
meals(beeflomein, dinner).

contain(roastbeefonweck, beef).
meals(roastbeefonweck, lunch).
meals(roastbeefonweck, dinner).

contain(moroccanbeefstew, beef).
meals(moroccanbeefstew, lunch).
meals(moroccanbeefstew, dinner).

contain(beeftenderloinwithportsauce, beef).
meals(beeftenderloinwithportsauce, lunch).
meals(beeftenderloinwithportsauce, dinner).

contain(italianstylebeefstew, beef).
meals(italianstylebeefstew, lunch).
meals(italianstylebeefstew, dinner).

contain(corianderdustedroastbeef, beef).
meals(corianderdustedroastbeef, lunch).
meals(corianderdustedroastbeef, dinner).

contain(beefstroganoff, beef).
meals(beefstroganoff, lunch).
meals(beefstroganoff, dinner).

contain(pepperedbeefstroganoff, beef).
meals(pepperedbeefstroganoff, lunch).
meals(pepperedbeefstroganoff, dinner).

contain(beefkreplach, beef).
meals(beefkreplach, lunch).
meals(beefkreplach, dinner).

contain(basquebeeftenderloin, beef).
meals(basquebeeftenderloin, lunch).
meals(basquebeeftenderloin, dinner).

contain(chipotleshreddedbeef, beef).
meals(chipotleshreddedbeef, lunch).
meals(chipotleshreddedbeef, dinner).

contain(beefandbaconpie, beef).
meals(beefandbaconpie, lunch).
meals(beefandbaconpie, dinner).

contain(smokedbeefbrisket, beef).
meals(smokedbeefbrisket, lunch).
meals(smokedbeefbrisket, dinner).

contain(roastbeefsandwiches, beef).
meals(roastbeefsandwiches, lunch).
meals(roastbeefsandwiches, dinner).

contain(beefstew, beef).
meals(beefstew, lunch).
meals(beefstew, dinner).

contain(beefstroganoff, beef).
meals(beefstroganoff, lunch).
meals(beefstroganoff, dinner).

contain(friedbeefwithbroccolirecipes, beef).
meals(friedbeefwithbroccolirecipes, lunch).
meals(friedbeefwithbroccolirecipes, dinner).

contain(mediterraneanbeefstew, beef).
meals(mediterraneanbeefstew, lunch).
meals(mediterraneanbeefstew, dinner).

contain(beefstew, beef).
meals(beefstew, lunch).
meals(beefstew, dinner).

contain(pepperedbeeftenderloinrecipes, beef).
meals(pepperedbeeftenderloinrecipes, lunch).
meals(pepperedbeeftenderloinrecipes, dinner).

contain(roastbeeftenderloin, beef).
meals(roastbeeftenderloin, lunch).
meals(roastbeeftenderloin, dinner).

contain(beefstewrecipes, beef).
meals(beefstewrecipes, lunch).
meals(beefstewrecipes, dinner).

contain(easybeefbrisket, beef).
meals(easybeefbrisket, lunch).
meals(easybeefbrisket, dinner).

contain(macaroniwithbeef, beef).
meals(macaroniwithbeef, lunch).
meals(macaroniwithbeef, dinner).

contain(beeftenderloinstew, beef).
meals(beeftenderloinstew, lunch).
meals(beeftenderloinstew, dinner).

contain(beefandpotatopie, beef).
meals(beefandpotatopie, lunch).
meals(beefandpotatopie, dinner).

contain(salmonrillettessalmonspread, salmon).
meals(salmonrillettessalmonspread, lunch).
meals(salmonrillettessalmonspread, dinner).

contain(ikurasalmoncaviar, salmon).
meals(ikurasalmoncaviar, lunch).
meals(ikurasalmoncaviar, dinner).

contain(salmonrillette, salmon).
meals(salmonrillette, lunch).
meals(salmonrillette, dinner).

contain(theslamminsalmonsandwich, salmon).
meals(theslamminsalmonsandwich, lunch).
meals(theslamminsalmonsandwich, dinner).

contain(doublesalmondip, salmon).
meals(doublesalmondip, lunch).
meals(doublesalmondip, dinner).

contain(salmonsaladwithchivesrecipes, salmon).
meals(salmonsaladwithchivesrecipes, lunch).
meals(salmonsaladwithchivesrecipes, dinner).

contain(quickmealpaprikaroastedsalmon, salmon).
meals(quickmealpaprikaroastedsalmon, lunch).
meals(quickmealpaprikaroastedsalmon, dinner).

contain(salmonrillettes, salmon).
meals(salmonrillettes, lunch).
meals(salmonrillettes, dinner).

contain(grilledsalmon, salmon).
meals(grilledsalmon, lunch).
meals(grilledsalmon, dinner).

contain(redpistousalmon, salmon).
meals(redpistousalmon, lunch).
meals(redpistousalmon, dinner).

contain(salmonspread, salmon).
meals(salmonspread, lunch).
meals(salmonspread, dinner).

contain(salmonteriyaki, salmon).
meals(salmonteriyaki, lunch).
meals(salmonteriyaki, dinner).

contain(salmonrillettes, salmon).
meals(salmonrillettes, lunch).
meals(salmonrillettes, dinner).

contain(salmonrillettesrecipe, salmon).
meals(salmonrillettesrecipe, lunch).
meals(salmonrillettesrecipe, dinner).

contain(zestybalsamicsalmon, salmon).
meals(zestybalsamicsalmon, lunch).
meals(zestybalsamicsalmon, dinner).

contain(broiledsalmon, salmon).
meals(broiledsalmon, lunch).
meals(broiledsalmon, dinner).

contain(wasabisalmon, salmon).
meals(wasabisalmon, lunch).
meals(wasabisalmon, dinner).

contain(mustardsalmon, salmon).
meals(mustardsalmon, lunch).
meals(mustardsalmon, dinner).

contain(smokedsalmonsaladsandwich, salmon).
meals(smokedsalmonsaladsandwich, lunch).
meals(smokedsalmonsaladsandwich, dinner).

contain(honeymustardglazedsalmonsteaksrecipes, salmon).
meals(honeymustardglazedsalmonsteaksrecipes, lunch).
meals(honeymustardglazedsalmonsteaksrecipes, dinner).

contain(salmonkabayakirecipes, salmon).
meals(salmonkabayakirecipes, lunch).
meals(salmonkabayakirecipes, dinner).

contain(salmonrillettes, salmon).
meals(salmonrillettes, lunch).
meals(salmonrillettes, dinner).

contain(pottedfreshsmokedsalmon, salmon).
meals(pottedfreshsmokedsalmon, lunch).
meals(pottedfreshsmokedsalmon, dinner).

contain(orangejalapenosalmon, salmon).
meals(orangejalapenosalmon, lunch).
meals(orangejalapenosalmon, dinner).

contain(salmonwithleeks, salmon).
meals(salmonwithleeks, lunch).
meals(salmonwithleeks, dinner).

contain(grilledsalmonskewers, salmon).
meals(grilledsalmonskewers, lunch).
meals(grilledsalmonskewers, dinner).

contain(ranchsalmonburgers, salmon).
meals(ranchsalmonburgers, lunch).
meals(ranchsalmonburgers, dinner).

contain(exceptionalsalmon, salmon).
meals(exceptionalsalmon, lunch).
meals(exceptionalsalmon, dinner).

contain(salmonrillettes, salmon).
meals(salmonrillettes, lunch).
meals(salmonrillettes, dinner).

contain(teriyakisalmon, salmon).
meals(teriyakisalmon, lunch).
meals(teriyakisalmon, dinner).

contain(easycrumblymustardlysalmon, salmon).
meals(easycrumblymustardlysalmon, lunch).
meals(easycrumblymustardlysalmon, dinner).

contain(searedthaisalmon, salmon).
meals(searedthaisalmon, lunch).
meals(searedthaisalmon, dinner).

contain(salmoncroquettes, salmon).
meals(salmoncroquettes, lunch).
meals(salmoncroquettes, dinner).

contain(maplebaconsalmon, salmon).
meals(maplebaconsalmon, lunch).
meals(maplebaconsalmon, dinner).

contain(chipotlelimesalmonrecipes, salmon).
meals(chipotlelimesalmonrecipes, lunch).
meals(chipotlelimesalmonrecipes, dinner).

contain(maplewalnutsalmon, salmon).
meals(maplewalnutsalmon, lunch).
meals(maplewalnutsalmon, dinner).

contain(roastedsalmon, salmon).
meals(roastedsalmon, lunch).
meals(roastedsalmon, dinner).

contain(salmononigiri, salmon).
meals(salmononigiri, lunch).
meals(salmononigiri, dinner).

contain(potatosalmongrill, salmon).
meals(potatosalmongrill, lunch).
meals(potatosalmongrill, dinner).

contain(smokedsalmonspread, salmon).
meals(smokedsalmonspread, lunch).
meals(smokedsalmonspread, dinner).

contain(openfacedsalmonsandwich, salmon).
meals(openfacedsalmonsandwich, lunch).
meals(openfacedsalmonsandwich, dinner).

contain(panfriedsalmon, salmon).
meals(panfriedsalmon, lunch).
meals(panfriedsalmon, dinner).

contain(salmontartarerecipe, salmon).
meals(salmontartarerecipe, lunch).
meals(salmontartarerecipe, dinner).

contain(salmonrillette, salmon).
meals(salmonrillette, lunch).
meals(salmonrillette, dinner).

contain(teriyakisalmon, salmon).
meals(teriyakisalmon, lunch).
meals(teriyakisalmon, dinner).

contain(buffalosalmon, salmon).
meals(buffalosalmon, lunch).
meals(buffalosalmon, dinner).

contain(spicyglazedsalmon, salmon).
meals(spicyglazedsalmon, lunch).
meals(spicyglazedsalmon, dinner).

contain(potatosalmongrill, salmon).
meals(potatosalmongrill, lunch).
meals(potatosalmongrill, dinner).

contain(salmonwithcucumbers, salmon).
meals(salmonwithcucumbers, lunch).
meals(salmonwithcucumbers, dinner).

contain(summersalmon, salmon).
meals(summersalmon, lunch).
meals(summersalmon, dinner).

contain(chilirubbedsalmon, salmon).
meals(chilirubbedsalmon, lunch).
meals(chilirubbedsalmon, dinner).

contain(citrussalmonsalad, salmon).
meals(citrussalmonsalad, lunch).
meals(citrussalmonsalad, dinner).

contain(chipotlelimesalmon, salmon).
meals(chipotlelimesalmon, lunch).
meals(chipotlelimesalmon, dinner).

contain(salmonmousse, salmon).
meals(salmonmousse, lunch).
meals(salmonmousse, dinner).

contain(gingerlimesalmon, salmon).
meals(gingerlimesalmon, lunch).
meals(gingerlimesalmon, dinner).

contain(zestysalmon, salmon).
meals(zestysalmon, lunch).
meals(zestysalmon, dinner).

contain(pepperysalmoncakes, salmon).
meals(pepperysalmoncakes, lunch).
meals(pepperysalmoncakes, dinner).

contain(micropoachedsalmon, salmon).
meals(micropoachedsalmon, lunch).
meals(micropoachedsalmon, dinner).

contain(microwavesalmon, salmon).
meals(microwavesalmon, lunch).
meals(microwavesalmon, dinner).

contain(sweetandsoursalmon, salmon).
meals(sweetandsoursalmon, lunch).
meals(sweetandsoursalmon, dinner).

contain(smokedsalmonquesadillas, salmon).
meals(smokedsalmonquesadillas, lunch).
meals(smokedsalmonquesadillas, dinner).

contain(smokedsalmonspread, salmon).
meals(smokedsalmonspread, lunch).
meals(smokedsalmonspread, dinner).

contain(marinatedsalmon, salmon).
meals(marinatedsalmon, lunch).
meals(marinatedsalmon, dinner).

contain(salmonavocadocucumbersalad, salmon).
meals(salmonavocadocucumbersalad, lunch).
meals(salmonavocadocucumbersalad, dinner).

contain(honeyglazedsalmon, salmon).
meals(honeyglazedsalmon, lunch).
meals(honeyglazedsalmon, dinner).

contain(simplepoachedsalmon, salmon).
meals(simplepoachedsalmon, lunch).
meals(simplepoachedsalmon, dinner).

contain(salmonavocadocucumbersalad, salmon).
meals(salmonavocadocucumbersalad, lunch).
meals(salmonavocadocucumbersalad, dinner).

contain(slowroastedsalmon, salmon).
meals(slowroastedsalmon, lunch).
meals(slowroastedsalmon, dinner).

contain(asiansalmon, salmon).
meals(asiansalmon, lunch).
meals(asiansalmon, dinner).

contain(minisalmoncakes, salmon).
meals(minisalmoncakes, lunch).
meals(minisalmoncakes, dinner).

contain(braidedsalmon, salmon).
meals(braidedsalmon, lunch).
meals(braidedsalmon, dinner).

contain(smokedsalmontriangles, salmon).
meals(smokedsalmontriangles, lunch).
meals(smokedsalmontriangles, dinner).

contain(chardwrappedsalmon, salmon).
meals(chardwrappedsalmon, lunch).
meals(chardwrappedsalmon, dinner).

contain(beersalmon, salmon).
meals(beersalmon, lunch).
meals(beersalmon, dinner).

contain(salmonflorentine, salmon).
meals(salmonflorentine, lunch).
meals(salmonflorentine, dinner).

contain(searedsalmonfillet, salmon).
meals(searedsalmonfillet, lunch).
meals(searedsalmonfillet, dinner).

contain(scrambledeggswithsmokedsalmon, salmon).
meals(scrambledeggswithsmokedsalmon, lunch).
meals(scrambledeggswithsmokedsalmon, dinner).

contain(summertimesalmoncakesrecipes, salmon).
meals(summertimesalmoncakesrecipes, lunch).
meals(summertimesalmoncakesrecipes, dinner).

contain(lightlysmokedsalmon, salmon).
meals(lightlysmokedsalmon, lunch).
meals(lightlysmokedsalmon, dinner).

contain(grilledsalmonpaprika, salmon).
meals(grilledsalmonpaprika, lunch).
meals(grilledsalmonpaprika, dinner).

contain(salmonblt, salmon).
meals(salmonblt, lunch).
meals(salmonblt, dinner).

contain(salmonwiththymebutter, salmon).
meals(salmonwiththymebutter, lunch).
meals(salmonwiththymebutter, dinner).

contain(bakedsalmoneggs, salmon).
meals(bakedsalmoneggs, lunch).
meals(bakedsalmoneggs, dinner).

contain(cardamommaplesalmon, salmon).
meals(cardamommaplesalmon, lunch).
meals(cardamommaplesalmon, dinner).

contain(poachedsalmonwithleeks, salmon).
meals(poachedsalmonwithleeks, lunch).
meals(poachedsalmonwithleeks, dinner).

contain(marinatedsalmon, salmon).
meals(marinatedsalmon, lunch).
meals(marinatedsalmon, dinner).

contain(prosciuttowrappedsalmon, salmon).
meals(prosciuttowrappedsalmon, lunch).
meals(prosciuttowrappedsalmon, dinner).

contain(roastedsalmonandgrapetomatoes, salmon).
meals(roastedsalmonandgrapetomatoes, lunch).
meals(roastedsalmonandgrapetomatoes, dinner).

contain(simplepoachedsalmon, salmon).
meals(simplepoachedsalmon, lunch).
meals(simplepoachedsalmon, dinner).

contain(lemonygrilledsalmon, salmon).
meals(lemonygrilledsalmon, lunch).
meals(lemonygrilledsalmon, dinner).

contain(salmonenpapillote, salmon).
meals(salmonenpapillote, lunch).
meals(salmonenpapillote, dinner).

contain(panfriedsalmon, salmon).
meals(panfriedsalmon, lunch).
meals(panfriedsalmon, dinner).

contain(anticrampsalmonrecipes, salmon).
meals(anticrampsalmonrecipes, lunch).
meals(anticrampsalmonrecipes, dinner).

contain(salmonfilletsovercouscous, salmon).
meals(salmonfilletsovercouscous, lunch).
meals(salmonfilletsovercouscous, dinner).

contain(soyglazedsalmonsteaks, salmon).
meals(soyglazedsalmonsteaks, lunch).
meals(soyglazedsalmonsteaks, dinner).

contain(pancettawrappedsalmon, salmon).
meals(pancettawrappedsalmon, lunch).
meals(pancettawrappedsalmon, dinner).

contain(tangyteriyakisalmon, salmon).
meals(tangyteriyakisalmon, lunch).
meals(tangyteriyakisalmon, dinner).

same_main_ingredient(X,Y) :-  contain(X,Z), contain(Y,Z).
same_meals(X,Y) :-  meals(X,Z), meals(Y,Z).
sibling_food(X,Y) :- same_main_ingredient(X,Y), same_meals(X,Y).
checkCalorie(Food,Target) :- Food =< Target + 100, Food >= Target - 200.
