:- dynamic isenough/1.

% KNOWLEDGE BASE

% isenough(x) will check whether the user is getting enough sleep
% X = number of hours
isenough(X) :- (X>=7), (X=<9).