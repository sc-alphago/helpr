:- dynamic get_vid/3.

% KNOWLEDGE BASE

% level(x,y) means "video y is a x level exercise"
level(light, vid1).
level(light, vid4).
level(light, vid7).
level(light, vid8).
level(medium, vid2).
level(medium, vid5).
level(medium, vid9).
level(medium, vid11).
level(medium, vid12).
level(medium, vid14).
level(medium, vid15).
level(medium, vid18).
level(intense, vid3).
level(intense, vid6).
level(intense, vid10).
level(intense, vid13).
level(intense, vid16).
level(intense, vid17).
level(intense, vid19).
level(intense, vid20).

% contains(x,y) means "video y contains x as one of its moves"
contains(jump, vid3).
contains(squat, vid2).
contains(squat, vid3).
contains(pushup, vid4).
contains(pushup, vid6).
contains(twist, vid1).
contains(twist, vid5).
contains(twist, vid8).
contains(twist, vid16).
contains(stretch, vid8).
contains(stretch, vid9).
contains(fold, vid8).
contains(dance, vid10).
contains(dance, vid11).
contains(dance, vid12).
contains(dance, vid13).
contains(dance, vid15).
contains(dance, vid19).
contains(dance, vid20).
contains(plank, vid16).


% video recommendation logic
get_vid(Video, Level, Exclusion) :- level(Level, Video), not(contains(Exclusion, Video)).