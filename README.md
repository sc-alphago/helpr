<div align="center">
<p>
  <a href="http://sc-helpr.herokuapp.com/">
    <img src="https://img.shields.io/badge/%20%F0%9F%92%BB-website-blue">
  </a>
  <a href="https://docs.google.com/document/d/1ys-2eXQmSOt2ezhrZpd7viOxdt7pN6DYZGqH8jf5wdY/edit?usp=sharing">
    <img src="https://img.shields.io/badge/%F0%9F%93%91-Proposal-blue" >
  </a>
   <a href="https://tinyurl.com/sc-kba">
    <img src="https://img.shields.io/badge/%F0%9F%93%A5-design-blue" >
  </a>
  
</p>

<br>

<h1> 🍉 About Helpr 🍉 </h1>
<p text-align= 'center' >Helpr is a web-based healthy lifestyle planner app that gives users a personalized recommendation on meal plan, physical exercise, and proportional rest hours.  When you open the site, we will ask you about your physical attributes, age, lifestyle, food and exercise preference and constraint. Based on those input, the AI algoritm will find the most suitable output according to a given set of inference rules and the available knowledge-base which will be explained further in the following part. </p>
<p> The stack we used: Pyswip ( Python Prolog Library ), Python Flask </p>
<br>
<a href="https://ibb.co/xKZRf3H"><img src="https://i.ibb.co/fVJPnQv/interface.jpg" alt="interface" border="0"></a>
<br>
<br>
<a href="https://ibb.co/rp4Y89C"><img src="https://i.ibb.co/GMcyYmK/result.jpg" alt="result" border="0"></a>

<h1 align="center"> ️💌 About Us 💌 </h1>
Group Name : Alpha Go

**Developers**

◙ [Fathinah Asma Izzati 1806235744](https://gitlab.com/fathinah)<br>
◙ [Nabila Ayu Dewanti 1806205312](https://gitlab.com/nabilaayu)<br>
◙ [Thami Endamora 1806141460](https://gitlab.com/thami.endamora)<br>
◙ [Shafiya Ayu Adzhani 1806235845](https://gitlab.com/adzshaf)<br>

We welcome any feedbacks and suggestions.


<h1 align="center"> ️📍  AI Behind Helpr : Knowledge Based Agent 📍  </h1>
The AI agent implements inference technique called backward chaining. It applies backward reasoning method, which initially starting from the goal state, when doing the inference process. A backward chaining algorithm is a form of reasoning, which starts with the goal and works backward, chaining through rules to find known facts that support the goal. In backward chaining, the goal is broken into sub-goal or sub-goals to prove the facts true thus it is called a goal-driven approach.
<br>
Backward-chaining is based on modus ponens inference rule.<br>
![image.png](./image.png)
<br>
After provinding some input, program will run 3 inference procedures:
<br>

***Meal plan recommender***<br>
Variable: Calorie intake, favorite breakfast, lunch, dinner  <br>
Predicate: contains(Y,Z) <br>
Function: meals(X,Y), same_main_ingredient(X,Y), checkCalorie(Food,Target), sibling_food(X,Y) <br>
>  Output: return three sets of meal which contain the same ingredient as his/her favorite (sibling food) at the right time (morning/noon/evening) and fulfill the calorie constraint.


***Exercise suggestion***<br>
Variable: Calorie intake, favorite breakfast, lunch, dinner  <br>
Predicate: - <br> 
Function: level(Level, Video), contains(Exclusion, Video), get_vid(Video, Level, Exclusion) <br>
>  Output: return the most suitable exercises video based on his/her exclusion request and level.


***Sleep duration suggestion***<br>
Variable: sleep length  <br>
Predicate: - <br> 
Function: isenough(X) <br>
>  Output: return sleeping hours to add/reduce based on current sleep length.

<h1 align="center"> 🚩 Set Up Locally 🚩 </h1>

Virtual Environment and Dependencies 
Prerequisite: python and git installed.
```
git clone https://gitlab.com/sc-alphago/helpr
cd help/
python -r requirements.txt
set FLASK_APP=app.py
flask run


```
</div>

