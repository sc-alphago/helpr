from pyswip import *
import json
from pyswip_mt import PrologMT

with open('./exercise_sleep/videodb.json') as jsondata:
  data = json.load(jsondata)
  
def get_rec(rec_id):
    for vid in data:
        if vid.get("Id") == rec_id:
            return {"title": vid.get("Title"), "link": vid.get("Link")}
            # return vid.get("Title") + " ("+vid.get("Link")+")"

def tell_activity(level, exclusion):
    prolog = PrologMT()
    prolog.consult("./exercise_sleep/videologic.pl")

    query_result = list(prolog.query("get_vid(Video, "+level+", "+exclusion+")"))
    
    result = []
    for video in query_result:
      rec_id = video.get("Video")
      result.append(get_rec(rec_id))

    return result

def main(level, activity_type):
    result = {}
    recommendation = tell_activity(level, activity_type)

    result["activity"] = recommendation
    return result

if __name__ == '__main__':
    main()