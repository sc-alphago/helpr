from pyswip import *
from pyswip_mt import PrologMT

def tell_sleep_hour(user_input):
    prolog = PrologMT()
    prolog.consult("./exercise_sleep/sleeplogic.pl")

    test = bool(list(prolog.query("isenough(" + user_input + ")")))
    print(test)
    if test:
        return "Your sleeping duration is sufficient"
    elif int(user_input) > 9:
        return "You need to reduce at least " + str(int(user_input) - 9) + " hours of sleep"
    else:
        return "You need to add " + str(7 - int(user_input)) + " hours of sleep"

def main(user_input):
    result = {}
    message = tell_sleep_hour(user_input)

    result["sleep"] = message
    return result

if __name__ == '__main__':
    main()
