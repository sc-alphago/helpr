from flask import Flask, render_template, request, jsonify, make_response
import json
import main
import sleep
import video_logic

app = Flask(__name__)


@app.route("/")
def index(name=None):
    return render_template("index.html", name=name)


@app.route("/result", methods=["POST"])
def show_result(name=None):
    req = request.get_json()
    if req["gender"] == "Laki-laki":
        calories = (
                10 * int(req["weight"])
                + 6.25 * int(req["height"])
                - 5 * int(req["age"])
                + 5
        )
    else:
        calories = (
                10 * int(req["weight"])
                + 6.25 * int(req["height"])
                - 5 * int(req["age"])
                + 161
        )

    if req["intensity"] == "Tidak pernah bergerak":
        calories = calories * 1.2
    elif req["intensity"] == "Aktif (Ringan)":
        calories = calories * 1.375
    elif req["intensity"] == "Aktif (Sedang)":
        calories = calories * 1.55
    elif req["intensity"] == "Sangat aktif":
        calories = calories * 1.725
    elif req["intensity"] == "Aktif (Ekstra)":
        calories = calories * 1.9
    result = main.main(
        calories, req["breakfast"], req["lunch"], req["dinner"]
    )

    sleep_hour = sleep.main(req["sleep"])
    video_result = video_logic.main(req["level"], req["type"])

    result.update(sleep_hour)
    result.update(video_result)

    response = make_response(jsonify(result), 200)
    return response


@app.route("/video")
def video(name=None):
    with open("./app/videodb.json") as _data:
        data = json.load(_data)
    res = make_response(jsonify(data), 200)
    return res


@app.route("/food/breakfast")
def breakfast(name=None):
    with open("./food/breakfast.json") as _data:
        data = json.load(_data)
    res = make_response(jsonify(data), 200)
    return res


@app.route("/food/lunch")
def lunch(name=None):
    with open("./food/lunch.json") as _data:
        data = json.load(_data)
    res = make_response(jsonify(data), 200)
    return res


@app.route("/food/dinner")
def dinner(name=None):
    with open("./food/dinner.json") as _data:
        data = json.load(_data)
    res = make_response(jsonify(data), 200)
    return res


@app.route("/activity/level")
def activity_level(name=None):
    intensity_level = [
        {"id": 0, "name": "light"},
        {"id": 1, "name": "medium"},
        {"id": 3, "name": "intense"},
    ]
    res = make_response(jsonify(intensity_level), 200)
    return res


@app.route("/activity/type")
def activity_type(name=None):
    exercise_move = [
        {"id": 0, "name": "none"},
        {"id": 1, "name": "jump"},
        {"id": 3, "name": "squat"},
        {"id": 4, "name": "pushup"},
        {"id": 5, "name": "twist"},
    ]
    res = make_response(jsonify(exercise_move), 200)
    return res


@app.route("/testtest")
def testresult(name=None):
    cal = 10 * 42 + 6.25 * 157 - 5 * 20 + 161
    cal = cal * 1.375
    result = main.main(cal, "anadamabread", "beeftacos", "roastsirloinofbeef")
    print(result)
    return 'hello'
