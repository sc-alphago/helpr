from pyswip import Prolog
from pyswip_mt import PrologMT
import random
import json

prolog = PrologMT()
prolog.consult('./food/knowledge_food.pl')


def check_sibling_food(user_input):
    food_list = []
    for results in prolog.query("sibling_food(" + user_input + ",Y)"):
        if results['Y'] not in food_list:
            food_list.append(results['Y'])
    return food_list


def check_food_calorie(food_list, user_need):
    food_database = open('./food/food_data.json', "r")
    data = json.load(food_database)

    recommended_food = []
    for food in food_list:
        food_data = data['food'][food]
        food_calorie = food_data['calorie']
        if bool(list(prolog.query("checkCalorie(" + str(food_calorie) + "," + str(user_need) + ")"))):
            recommended_food.append(food_data)
    return recommended_food


def tell_recommended_food(meals, user_input, user_need):
    food_choice_details = {"meals": meals}
    food_list = check_sibling_food(user_input)
    recommended_food = check_food_calorie(food_list, user_need)
    if len(recommended_food) == 0:
        return []

    choice = random.choice(recommended_food)
    for detail in choice:
        choice_items = {detail: choice[detail]}
        food_choice_details.update(choice_items)

    return food_choice_details


def main(calorie, breakfast, lunch, dinner):
    meals_result = []
    result = {}
    calorie_per_meals = calorie // 3

    breakfast_result = tell_recommended_food('Breakfast', breakfast, calorie_per_meals)
    meals_result.append(breakfast_result)

    lunch_result = tell_recommended_food('Lunch', lunch, calorie_per_meals)
    meals_result.append(lunch_result)

    dinner_result = tell_recommended_food('Dinner', dinner, calorie_per_meals)
    meals_result.append(dinner_result)

    result["food"] = meals_result
    return result
