import json
import requests


def configure_food_api():
    configure_prolog_header()

    food_database = {}
    food_database['food'] = {}

    food_api = 'https://api.edamam.com/search?app_id=1454d1d2&app_key=3a7eade2e5469051698e45da32e35df9&from=0&to=100&calories=200-1000&q='
    ingr_breakfast = ['rice', 'bread', 'cereal']
    ingr_lunchdinner = ['chicken', 'beef', 'salmon']
    for ingr in ingr_breakfast:
        url = food_api + ingr
        data = json.loads(requests.get(url).text)
        configure_database(data, ingr, 'breakfast', food_database['food'])

    for ingr in ingr_lunchdinner:
        url = food_api + ingr
        data = json.loads(requests.get(url).text)
        configure_database(data, ingr, 'lunch', food_database['food'])
        configure_database(data, ingr, 'dinner', food_database['food'])

    configure_logic()
    with open('./food/food_data.json', 'a+') as outfile:
        json.dump(food_database, outfile, indent=4)


def configure_database(json_data, ingredient, meal_type, jsonoutput):
    f = open('./food/knowledge_food.pl', "a+")
    food_data = json_data['hits']
    for data in food_data:
        food = data['recipe']
        food_name = food['label']
        food_id = ''.join([i for i in food_name.lower() if i.isalpha()])
        food_image = food['image']
        food_cal = int(food['calories']) / int(food['yield'])
        food_carbs = int(food['totalNutrients']['CHOCDF']['quantity']) / int(food['yield'])
        food_fat = int(food['totalNutrients']['FAT']['quantity']) / int(food['yield'])
        food_protein = int(food['totalNutrients']['PROCNT']['quantity']) / int(food['yield'])
        food_links = food['url']

        jsonoutput[food_id] = {
            'name': food_name,
            'image': food_image,
            'calorie': food_cal,
            'carbs': food_carbs,
            'fat': food_fat,
            'protein': food_protein,
            'links': food_links
        }

        f.write("contains(" + food_id + ", " + ingredient + ").\n")
        f.write("meals(" + food_id + ", " + meal_type + ").\n")
        f.write("meals(" + food_id + ", dinner).\n")
        f.write("\n")
    f.close()


def configure_prolog_header():
    # Write rules definition to knowledge_food.pl
    f = open('./food/knowledge_food.pl', "w")
    f.write(":- discontiguous contains/2.\n")
    f.write(":- discontiguous meals/2.\n\n")
    f.close()


def configure_logic():
    # Write rules to knowledge_food.pl
    f = open('./food/knowledge_food.pl', "a+")
    f.write("same_main_ingredient(X,Y) :-  contains(X,Z), contains(Y,Z).\n")
    f.write("same_meals(X,Y) :-  meals(X,Z), meals(Y,Z).\n")
    f.write("sibling_food(X,Y) :- same_main_ingredient(X,Y), same_meals(X,Y).\n")
    f.write("checkCalorie(Food,Target) :- Food =< Target, Food >= Target - 300.\n")
    f.close()
