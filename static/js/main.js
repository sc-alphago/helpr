function toggleActive(index) {
  let toggleTarget = document.getElementsByClassName("dropdown")[`${index}`];
  toggleTarget.classList.toggle("is-active");
}

function selectOption(value, className, name) {
  let select = document.getElementsByClassName(className)["0"];
  select.innerHTML = value.innerHTML;
  select.name = name;
}

function createNode(element) {
  return document.createElement(element); // Create the type of element you pass in the parameters
}

function append(parent, el) {
  return parent.appendChild(el); // Append the second parameter(element) to the first one
}

function onLoad() {
  getData("food","breakfast");
  getData("food","lunch");
  getData("food","dinner");
  getData("activity", "level");
  getData("activity", "type");
}

function getData(category, type) {
  const parentDiv = document.getElementById(`dropdown-${type}`);
  fetch(`${window.origin}/${category}/${type}`, {
    method: "GET",
    credentials: "include",
    cache: "no-cache",
    headers: new Headers({
      "content-type": "application/json",
    }),
  })
    .then(function (response) {
      if (response.status !== 200) {
        console.log(
          `Looks like there was a problem. Status code: ${response.status}`
        );
        return;
      }
      response.json().then(function (data) {
        let food = data;
        food.map(function (value) {
          let div = createNode("div");
          div.className = "dropdown-item";
          div.name = `${value.id}`;
          div.onclick = function () {
            selectOption(this, `value-${type}`, value.id);
          };
          div.innerHTML = `${value.name}`;
          append(parentDiv, div);
        });
      });
    })
    .catch(function (error) {
      console.log("Fetch error: " + error);
    });
}

function onSubmit() {
  const parentDiv = document.getElementById(`all-content`);
  let entry = {
    weight: document.getElementById("weight").value,
    height: document.getElementById("height").value,
    age: document.getElementById("age").value,
    gender: document
      .getElementsByClassName("value-gender")
      ["0"].innerHTML.trim(),
    intensity: document
      .getElementsByClassName("value-intensity")
      ["0"].innerHTML.trim(),
    breakfast: document.getElementsByClassName("value-breakfast")[0].name,
    lunch: document.getElementsByClassName("value-lunch")[0].name,
    dinner: document.getElementsByClassName("value-dinner")[0].name,
    level: document.getElementsByClassName("value-level")[0].textContent,
    type: document.getElementsByClassName("value-type")[0].textContent,
    sleep: document.getElementById("sleep").value,
  };

  fetch(`${window.origin}/result`, {
    method: "POST",
    credentials: "include",
    body: JSON.stringify(entry),
    cache: "no-cache",
    headers: new Headers({
      "content-type": "application/json",
    }),
  })
    .then(function (response) {
      if (response.status !== 200) {
        console.log(
          `Looks like there was a problem. Status code: ${response.status}`
        );
        return;
      }
      response.json().then(function (data) {
        document.getElementById("all-content").innerHTML = "";
        let div = createNode("div");
        div.className = "hero-body";

        let container = createNode("div");
        container.className = "container";


        let title = createNode("h1");
        title.innerHTML = "Result";
        title.className = "title has-text-centered";
        let foodDiv = createNode("div");

        for(let i = 0; i < data.food.length; i++) {
          let result = `<div class="mb-4"><b>${data.food[i]["meals"]}</b><p>${data.food[i]["name"]}</p><p>Calorie: ${data.food[i]["calorie"]}</p><p>Carbs: ${data.food[i]["carbs"]}</p><p>Fat: ${data.food[i]["fat"]}</p><p>Protein: ${data.food[i]["protein"]}</p><p><a href="${data.food[i]["links"]}">Recipe Link</a></p></div>`
          foodDiv.innerHTML += result
        }

        let sleepDiv = createNode("div");
        sleepDiv.innerHTML = "<b>Sleep</b>: " + data.sleep;

        let activityDiv = createNode("div");

        for(let i = 0; i< data.activity.length; i++) {
          let result = `<div class="mb-4"><p>${data.activity[i]["title"]}</p><iframe src=${data.activity[i]["link"]}></iframe></div>`
          activityDiv.innerHTML += result
        }

        append(container, title);
        append(container, foodDiv);
        append(container, sleepDiv);
        append(container, activityDiv);
        append(div, container);
        append(parentDiv, div);
      });
    })
    .catch(function (error) {
      console.log("Fetch error: " + error);
    });
}
